package Swagger;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static io.restassured.RestAssured.given;

public class OpenApiValidatorTest {

    private static final int PORT = 5555;
    private static final String SWAGGER_JSON_URL = "http://petstore.swagger.io/v2/swagger.json";

    private final OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(SWAGGER_JSON_URL);


    @Rule
    public WireMockRule wireMockRule = new WireMockRule(PORT);

    @Before
    public void setup(){
        wireMockRule.stubFor(
                WireMock.get(urlEqualTo("/pet/1"))
                .willReturn(aResponse()
                    .withStatus(200)
                    .withHeader("content-type", "application/json")
                    .withBody("{\"name\":\"fido\", \"photoUrls\":[]}")));

        wireMockRule.stubFor(
                WireMock.get(urlEqualTo("/pet/2"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("content-type", "application/json")
                                .withBody("{\"name\":\"fido\"}"))); // Missing required 'photoUrls' field

        wireMockRule.stubFor(
                WireMock.get(urlEqualTo("/pet/fido")) // Invalid petId
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("content-type", "application/json")
                                .withBody("{\"name\":\"fido\", \"photoUrls\":[]}")));

    }

    @Test
    public void testGetValidPet(){
        given().port(PORT)
                .filter(validationFilter)
                .header("api_key", "foo")
                .log().all()
                .when()
                .get("/pet/2")
                .then()
                .assertThat()
                .log().all()
                .statusCode(200);
    }


}
