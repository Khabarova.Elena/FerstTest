package RestAssured;

import RestAssured.DTO.User;
import RestAssured.DTO.UserOut;
import RestAssured.Service.UserApi;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;

public class PetStoreCreateUserTest {
    @Test
    public void createUserTest(){
        UserApi userApi = new UserApi();

        User user1 = new User(1,
                "UserName",
                "FirstName",
                "LastName",
                "email",
                "password",
                "phone",
                2);

        Response response = userApi.createUser(user1);

        response
                .then()
                .log().all()
                .statusCode(200)
                .body("code", equalTo(200))
                .body("type", equalTo("unknown"))
                .body("message", equalTo("1"))
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema/CreateUser.json"));


        //2 вариант для записи значения в переменную
        int actualCode = response.jsonPath().get("code");
        Assertions.assertEquals(200, actualCode);

        //3 вариант
        UserOut userOut = response.then().extract().body().as(UserOut.class);
        Assertions.assertEquals(200, userOut.getCode());
        Assertions.assertEquals("1", userOut.getMessage());



    }
}

