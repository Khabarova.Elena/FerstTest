package RestAssured.DTO;

/*{
        "code": 200,
        "type": "unknown",
        "message": "6874986966"
        }*/


import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class UserOut {
    private int code;
    private String type;
    private String message;
}
