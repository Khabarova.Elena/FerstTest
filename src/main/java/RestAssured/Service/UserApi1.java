package RestAssured.Service;

import RestAssured.DTO.User;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class UserApi1 {
    private final String BASE_URL = "https://petstore.swagger.io/v2";
    private final String USER = "/user";
    private RequestSpecification spec;

    public UserApi1(String baseUrl){
        spec = given()
                .baseUri(baseUrl)
                .contentType(ContentType.JSON);
    }


    public Response createUser(User user){
        return given(spec)
                .with()
                .body(user)
                .log().all()
              .when()
                .post(USER);
    }

    public Response getUser(){
        return given(spec)
                .with()
                .body("")
                .log().all()
                .when()
                .post(USER);
    }
}
