package Animals;

public abstract class Animal {
    private String name;
    private int age;

    public Animal(String name, int age){
        this.name = name;
        this.age = age;
    }




    public String getName() {
        return name;
    }


    public int getAge(int i) {
        return age;
    }

    public void setAge(int age) {
        if (age<0)
            this.age = age;
        else
            this.age = age;
    }
    public void happyBirthday(){
        age++;
    }



    public void Say(){
        System.out.println("Привет");
    }
}
