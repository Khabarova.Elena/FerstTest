package Animals;

public class Dog extends Animal{
    public Dog(String name, int age, int coutLeg) {
        super(name, age);
        this.coutLeg = coutLeg;
    }

    public int getCoutLeg(int i) {
        return coutLeg;
    }

    public void setCoutLeg(int coutLeg) {
        this.coutLeg = coutLeg;
    }

    private  int coutLeg;


    @Override
    public void Say() {
        System.out.println("Гав");
    }
}



