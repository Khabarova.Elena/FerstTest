/*{
        "operation_rtm30e": {
        "type": 8,
        "properties": null,
        "category_id": 2,
        "operation_info": {
        "oper_login": "Иванов А.А.",
        "linked_entity_info": null,
        "tara_info": null,
        "document_info": null,
        "bag_info": null,
        "rpo_info": {
        "base_info": {
        "links_list": null,
        "direct_ctg": 1,
        "create_address_data": {
        "type": 1,
        "properties": [],
        "index": "155134",
        "border_index": "123242"
        },
        "create_date_time": "2021-12-17 10:28:17",
        "create_time_zone": "+03:00",
        "bar_code": "15399733392943",
        "sys_id": {
        "date_time": "2021-12-17 10:31:13",
        "source_id": "EO",
        "po_version": "19.1.0.12552"
        },
        "entity_type": 1,
        "category_id": 3,
        "message_id": "ZTRACKING",
        "sender_id": "119620",
        "version_number": "2.11",
        "id": "15399733392943",
        "type": 47,
        "properties": [
        {
        "name": "track_by_group",
        "value": "0"
        }
        ],
        "date_processed": "2022-03-17 11:31:18"
        },
        "rpo_info": {
        "send_ctg": 1,
        "trans_type": 1,
        "post_mark": [
        0
        ],
        "mail_rank": 0,
        "trans_ctg": -1,
        "pay_type": [
        8
        ],
        "index_from": "123242",
        "index_to": "153996",
        "country_from": "643",
        "country_to": "643",
        "inter_type": -1,
        "weight": [
        {
        "type": 2,
        "value": 950.0,
        "measurement": "g"
        },
        {
        "type": 1,
        "value": 950.0,
        "measurement": "g"
        }
        ],
        "money": [
        {
        "type": 3,
        "properties": [],
        "value": 13804,
        "measurement": "RUB"
        },
        {
        "type": 4,
        "properties": [],
        "value": 4,
        "measurement": "RUB"
        },
        {
        "type": 6,
        "value": 3851,
        "measurement": "RUB"
        }

        ],
        "rcpn": {
        "type": 3,
        "properties": [
        {
        "name": "first_name",
        "value": "СОЛНЦЕВСКОГО"
        },
        {
        "name": "middle_name",
        "value": "Р-НА Г.МОСКВЫ"
        },
        {
        "name": "surname",
        "value": "ОВК"
        },
        {
        "name": "electronic_f22",
        "value": "0"
        },
        {
        "name": "phone",
        "value": "79874567329"
        }
        ],
        "name": "Тестовый Тест Тестович",
        "category_id": 1
        },
        "rcpn_address": {
        "type": 4,
        "properties": [
        {
        "name": "address_str",
        "value": "Г МОСКВА, ПР-КТ СОЛНЦЕВСКИЙ, Д. 3"
        }
        ],
        "index": "155134",
        "border_index": "119620",
        "address": {
        "type": 1,
        "region": "МОСКВА",
        "area": "",
        "place": "МОСКВА",
        "street": "ПР-КТ СОЛНЦЕВСКИЙ",
        "house": {
        "value": "3",
        "letter": "",
        "slash": "",
        "corpus": "",
        "building": "",
        "room": ""
        }
        },
        "country": {
        "a2_code": "RU",
        "a3_code": "RUS",
        "ru_code": "643"
        }
        },
        "sndr": {
        "type": 2,
        "properties": [
        {
        "name": "first_name",
        "value": "ГРУППА"
        },
        {
        "name": "middle_name",
        "value": "КОМПАНИЙ ПИК"
        },
        {
        "name": "surname",
        "value": "ПАО"
        },
        {
        "name": "send_ctg",
        "value": "1"
        },
        {
        "name": "electronic_f22",
        "value": "0"
        }
        ],
        "name": "ПАО ГРУППА КОМПАНИЙ ПИК",
        "category_id": 1
        },
        "sndr_address": {
        "type": 3,
        "properties": [
        {
        "name": "address_str",
        "value": "Г МОСКВА, УЛ БАРРИКАДНАЯ, Д. 19, СТР. 1"
        }
        ],
        "index": "155134",
        "address": {
        "type": 1,
        "region": "МОСКВА",
        "area": "",
        "place": "МОСКВА",
        "street": "УЛ БАРРИКАДНАЯ",
        "house": {
        "value": "19",
        "letter": "",
        "slash": "",
        "corpus": "",
        "building": "1",
        "room": ""
        }
        },
        "country": {
        "a2_code": "RU",
        "a3_code": "RUS",
        "ru_code": "643"
        }
        },
        "delivery": {
        "method": 2
        },
        "identification_method": 2,
        "package": {
        "kind": 3
        }
        }
        },
        "index_next": "155134",
        "type_of_operand1": 1,
        "oper_address_data": {
        "index": "155134"
        },
        "oper_time_zone": "+03:00",
        "oper_date_time": "2021-12-17 10:16:16",
        "sys_id": {
        "date_time": "2021-12-17 10:31:13",
        "source_id": "EO",
        "po_version": "19.1.0.12552"
        },
        "id_of_operand1": "15399733392943",
        "entity_bar_code": "15399733392943"
        }
        },
        "rpo_details": {
        "storage_location": null,
        "incoming_number": "МР-116",
        "delivery_number": "5"
        }
        }
*/




package RpoGeneration.DTO;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.schema.JsonSerializableSchema;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize


public class Spi {
    private int id;
    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String phone;
    private int userStatus;



}
