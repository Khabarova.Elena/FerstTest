package lombock;

import lombok.Data;
import lombok.NonNull;

@Data
public class Cat {
    @NonNull
    private String name;
    @NonNull
    private String famaly;
}
