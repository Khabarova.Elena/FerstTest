package CollectionLesson;

import java.util.*;

public class Main {

    public  static void main(String[] args) {
        /*List<String> list1 = new ArrayList<String>();
        ArrayList<String> list2 = new ArrayList<String>(list1);
        ArrayList<String> list3 = new ArrayList<String>(30);

        list1.add("Привет");
        list1.add(0, "World");

        list1.set(1, "Hello"); //array[1] = "Hello"

        System.out.println(list1);
        System.out.println(list1.isEmpty());

        String result = list1.get(1) + " " + list1.get(0); // String result = array[1] + "" + array[0];

        System.out.println(result);

        list1.remove("World");

        for(String el : list1){
            System.out.println(el);
        }

        ArrayDeque<String> eque = new ArrayDeque<String>();

            eque.addLast("Включи компьютер");
            eque.addLast("Зайди BIOS");
            eque.addLast("Поменять приоритет");

            System.out.println(eque);


            Integer i = 0;


            while(!eque.isEmpty()){
                String el = eque.pollFirst();
                System.out.println(el);
                i++;
                eque.addLast(i.toString());
            }

            System.out.println(eque);*/

        HashSet<String> set = new HashSet<String>();

        set.add("Russia");
        set.add("USA");
        set.add("Russia");

        System.out.println(set);


        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Hello", "Привет");
        map.put("World", "Мир");

        System.out.println(map.get("Hello") + " " + map.get("World"));



    }

    }

