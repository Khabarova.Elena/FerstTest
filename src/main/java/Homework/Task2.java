package Homework;

public class Task2 {
    public static void main(String[] args) {
        for (int a = 2; a <= 100; a++) {
            boolean isSimple = true;

            for (int b = 2; b < a; b++) {
                if (a % b == 0) {
                    isSimple = false;
                    break;
                }
            }

            if (isSimple) {
                System.out.println(a);
            }
        }


    }
}
