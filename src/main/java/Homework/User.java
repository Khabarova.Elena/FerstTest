package Homework;

public class User extends Man implements Loginable{

    private final String login;
    private final String password;
    private final String company;

    public User(String name, String surname, int age, String login, String password, String company) {
        super(name, surname, age);
        this.login = login;
        this.password = password;
        this.company = company;




    }

    @Override
    public void Say() {
        System.out.println("Я работаю в " + company);
    }

    @Override
    public void login() {
        System.out.println("Авторизация с логином " + login + " с паролем " + password);

    }
}
