package Homework;

import lombok.Getter;
import lombok.Setter;

public abstract class Man {
    @Getter
    @Setter
    protected String name;
    protected String surname;
    protected int age;

    public Man(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }


    public void Say() {
        System.out.println("Я говорю");
    }

    public void Go() {
        System.out.println("Я иду");
    }

    public void Drink() {
        System.out.println("Я пью");
    }

    public void Eat() {
        System.out.println("Я ем");
    }



}



