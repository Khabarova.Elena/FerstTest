package Patterns;

import lombok.Getter;
import lombok.Setter;


public class Singleton {
    private static Singleton instance;
    @Setter
    @Getter
    private String a;


    private Singleton () {};


    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}

